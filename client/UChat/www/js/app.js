/*
 * Please see the included README.md file for license terms and conditions.
 */


// This file is a suggested starting place for your code.
// It is completely optional and not required.
// Note the reference that includes it in the index.html file.


/*jslint browser:true, devel:true, white:true, vars:true */
/*global $:false, intel:false app:false, dev:false, cordova:false */



// This file contains your event handlers, the center of your application.
// NOTE: see app.initEvents() in init-app.js for event handler initialization code.

// function myEventHandler() {
//     "use strict" ;
// // ...event handler code here...
// }


// ...additional event handlers here...
function start () {
	$(function() {
		// Show default wechat tab content.
        $('#chatpage').hide();
        $('#mainpage').show();
		showTab('wechat');
		bindNaviTabEvents();
	});
}

function showTab (tabId) {
	// Hide all content tab.
	$('.content').hide();

	// Show the specified content.
	$('#' + tabId + '-content').show();
}

function bindNaviTabEvents () {
	
	$('.tab-item').click(function () {

		var tabId = $(this).attr('id');

		showTab(tabId);

	});
}
start();
    
$('#Stantz').click(function (){
   $('#mainpage').hide();
   $('#chatpage').show();
});
    
// 关闭聊天窗口
$(".chat-dialog .header .quit").on("click", function() {
        start();
});
    
// 回显聊天内容到聊天框中(发送代码)
$(".chat-dialog .send-message").on("click", function() {
        var text = $(".text-input-area textarea").val();
        if (text == "")
            return;
        addMessage(text);
        // 清空输入框和图片
        $(".text-input-area textarea").val("");
    });


    
function addMessage(text) {
    var html = '<div class="one-message">' +
                    '<div class="text-message"><pre>' + text + '</pre></div>'+
                '</div>';
    var $message = $(html);
    $(".message-display-area .messages").append($message);
}