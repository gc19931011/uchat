var express = require('express');
var bodyParser = require('body-parser');
var app = express();
app.use(bodyParser.json());
var expressWs = require('express-ws')(app);


// preserve all clients in an array
var clients = [];



app.get('/client.html', function(req, res) {
  res.sendFile(__dirname + '/client.html');
});


// log
var server = app.listen(3000,function(){
	var host = server.address().address;
	var port = server.address().port;
	console.log('chat server started on localhost:3000');
});


//To login, use POST.
app.post('/login',function(req,res){
	console.log(req.body);
	//search database match req.user
	//match req.password
	//if correct
	var ret = 
		"userexist": true,
		"passwordcorrect":true,
		"success": true
	};	
	res.send(ret);
	//else
	
});


//To register, use POST.
app.post('/register',function(req,res){
	console.log(req.body);
	//search database match req.user
	//if no exist username conflict,add to database
	var ret = 
		"userexist": true,
		"success": true
	};	
	res.send(ret);
	//else
	
});

//To chat,use WS
app.ws('/chat', function(ws, res) {
  // push new WebSocket client to client list
  clients.push(ws);


  //once get message from client
  ws.on('message', function(msg) {
	//find msg.receiver and forward msg to this client
	for(var i=0;i<clients.length;i++){
		if(clients[i]==msg.receiver)
			clients[i].send(msg);
	}
  });


  //once client close the connection,delete client
  ws.on('close', function() {
    throw new Error('Please implement your close handler');
  });

  //once an error has occured,send error msg to client
  ws.on('error', function() {
    ws.send('error');
  });
});